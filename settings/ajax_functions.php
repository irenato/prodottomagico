<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 15.11.16
 * Time: 10:18
 */


add_action('wp_ajax_nopriv_addContact', 'addContact');
add_action('wp_ajax_addContact', 'addContact');

function addContact()
{
    $email = stripcslashes(trim($_POST['email']));
    $first_name = stripcslashes(trim($_POST['first_name']));
    $last_name = stripcslashes(trim($_POST['last_name']));
    $phone = stripcslashes(trim($_POST['phone']));
    $list_id = get_field('active_campaign_list_id', $_POST['page']);
    return addContactToList($email, $first_name, $last_name, $phone, $list_id);
    die();
}

add_action('wp_ajax_nopriv_addTag', 'addTag');
add_action('wp_ajax_addTag', 'addTag');

/**
 * @return string
 */
function addTag()
{
    $email = stripcslashes(trim($_POST['email']));
    return addTagsToContact($email, get_field('tag_for_the_active_campaign', $_POST['page']));
    die();
}