<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 15.11.16
 * Time: 10:25
 */

/**
 * @param $email
 * @param $first_name
 * @param $last_name
 * @param $phone
 * @return string
 */
function addContactToList($email, $first_name, $last_name, $phone, $list_id)
{
    $url = 'https://brandlo.api-us1.com';


    $params = array(

        // the API Key can be found on the "Your Settings" page under the "API" tab.
        // replace this with your API Key
        'api_key' => 'b2f5e2f80ecde2a9bba357a6be3ca367dff18edaed91db1d35a71954cbc4c644d19aee84',

        // this is the action that adds a contact
        'api_action' => 'contact_add',
        'api_output' => 'serialize',
    );

// here we define the data we are posting in order to perform an update
    $post = array(
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone' => $phone,
        'orgname' => 'PM - Interessati Aspirapeli Magico',
        'tags' => 'api',

        // assign to lists:
        'p[7]' => $list_id, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
        'status[7]' => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
        'instantresponders[7]' => 1, // set to 0 to if you don't want to sent instant autoresponders
    );

// This section takes the input fields and converts them to the proper format
    $query = "";
    foreach ($params as $key => $value) $query .= $key . '=' . urlencode($value) . '&';
    $query = rtrim($query, '& ');

// This section takes the input data and converts it to the proper format
    $data = "";
    foreach ($post as $key => $value) $data .= $key . '=' . urlencode($value) . '&';
    $data = rtrim($data, '& ');

// clean up the url
    $url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
    if (!function_exists('curl_init')) die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
    if ($params['api_output'] == 'json' && !function_exists('json_decode')) {
        die('JSON not supported. (introduced in PHP 5.2.0)');
    }

// define a final API request - GET
    $api = $url . '/admin/api.php?' . $query;

    $request = curl_init($api); // initiate curl object
    curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
    curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
    curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

    $response = (string)curl_exec($request); // execute curl post and store results in $response

    curl_close($request); // close curl object

    if (!$response) {
        die('Nothing was returned. Do you have a connection to Email Marketing server?');
    }

    $result = unserialize($response);
    return $result['result_code'];
}

/**
 * @param $email
 * @param $tag
 */

function addTagsToContact($email, $tag)
{
    // By default, this sample code is designed to get the result from your ActiveCampaign installation and print out the result
    $url = 'http://brandlo.api-us1.com';


    $params = array(

        'api_key'      => 'b2f5e2f80ecde2a9bba357a6be3ca367dff18edaed91db1d35a71954cbc4c644d19aee84',
        'api_action'   => 'contact_tag_add',
        'api_output'   => 'serialize',
    );

// here we define the data we are posting in order to perform an update
    $post = array(
        'email' => $email, // contact email address (pass this OR the contact ID)
        'tags' => $tag,
    );

// This section takes the input fields and converts them to the proper format
    $query = "";
    foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
    $query = rtrim($query, '& ');

// This section takes the input data and converts it to the proper format
    $data = "";
    foreach( $post as $key => $value ) $data .= $key . '=' . urlencode($value) . '&';
    $data = rtrim($data, '& ');

// clean up the url
    $url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
    if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
    if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
        die('JSON not supported. (introduced in PHP 5.2.0)');
    }

// define a final API request - GET
    $api = $url . '/admin/api.php?' . $query;

    $request = curl_init($api); // initiate curl object
    curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
    curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
    curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

    $response = (string)curl_exec($request); // execute curl post and store results in $response

// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
    curl_close($request); // close curl object

    if ( !$response ) {
        die('Nothing was returned. Do you have a connection to Email Marketing server?');
    }

    $result = unserialize($response);
    return $result['result_code'];
}