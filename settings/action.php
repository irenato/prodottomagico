<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 28.11.16
 * Time: 15:26
 */

/**
 * @param $products_count
 * @param $page_id
 * @return mixed
 */

function filter_fb_pixels($products_count, $page_id)
{
    $pixels = get_field('fb_pixels', $page_id);
    if ($pixels)
        foreach ($pixels as $pixel) {
            if ($pixel['count_products'] == $products_count)
                return $pixel['links'];
        }
}

/**
 * @param $page_id
 * @return mixed
 */
function use_products_links($page_id)
{
    return get_field('links_to_product', $page_id);
}

/**
 * @param $page_slug
 * @return null
 */
function get_id_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

