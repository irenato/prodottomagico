<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 16.11.16
 * Time: 14:21
 */

//------------------------------------------
// отзывы
//------------------------------------------
add_action('init', 'register_reviews');
function register_reviews()
{
    register_post_type('reviews',
        array(
            'label' => __('Reviews'),
            'singular_label' => 'review ',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-status',
            'supports' => array(
                'title',
                'thumbnail',
                'editor'
            ),
        )
    );
}

if (function_exists("_p2p_load")) {
    function vp_post_to_post()
    {
        p2p_register_connection_type(array(
            'name' => 'products_to_reviews',
            'from' => 'page',
            'from_query_vars' => array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => 'products'
                    )
                )
            ),
            'to' => 'reviews',
        ));
    }

    add_action('p2p_init', 'vp_post_to_post');
}

add_action('init', 'category_for_pages');

function category_for_pages()
{
    register_taxonomy_for_object_type('category', 'page');
}