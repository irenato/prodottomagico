<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 14:59
 */


function prodottomagico_styles()
{
    wp_enqueue_style('prodottomagico_styles_bootstrap', get_template_directory_uri() . '/css/bootstrap-grid-3.3.1.min.css', '', '', 'all');
    wp_enqueue_style('prodottomagico_styles_font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', '', '', 'all');
    wp_enqueue_style('prodottomagico_styles_fonts', get_template_directory_uri() . '/css/fonts.css', '', '', 'all');
    wp_enqueue_style('prodottomagico_styles_main', get_template_directory_uri() . '/css/main.css', '', '', 'all');
    wp_enqueue_style('prodottomagico_styles_slick', get_template_directory_uri() . '/css/slick.css', '', '', 'all');
    wp_enqueue_style('prodottomagico_styles_slick-theme', get_template_directory_uri() . '/css/slick-theme.css', '', '', 'all');
}

add_action('wp_enqueue_scripts', 'prodottomagico_styles');

function prodottomagico_scripts()
{
    wp_enqueue_script('prodottomagico-bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', false, '', true);
    wp_enqueue_script('prodottomagico-jquery.maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', true);
//    wp_enqueue_script('prodottomagico-jquery-2.1.4.min', get_template_directory_uri() . '/js/jquery-2.1.4.min.js', false, '', true);
    wp_enqueue_script('prodottomagico-slick.min', get_template_directory_uri() . '/js/slick.min.js', false, '', true);
    wp_enqueue_script('prodottomagico-jquery.textchange', get_template_directory_uri() . '/js/jquery.textchange.js', false, '', true);
    if(is_page(8)){
        wp_enqueue_script('prodottomagico-common', get_template_directory_uri() . '/js/common.js', false, '', false);
    }else{
        wp_enqueue_script('prodottomagico-common', get_template_directory_uri() . '/js/action.js', false, '', false);
    }
    wp_localize_script( 'prodottomagico-common', 'prodottomagico_ajax', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action('wp_enqueue_scripts', 'prodottomagico_scripts');
