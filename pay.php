<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 13.12.16
 * Time: 10:59
 */

/**
 * Template name: Pay page
 */

get_header();

if (isset($_GET['prod'])) {
    $id = get_id_by_slug($_GET['prod']);
    $payment_block_items = get_field('payment_block', $id);
}

?>

    <section class="confirmation-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="confirmation">
                        <div class="description">
                            <img src="<?= get_template_directory_uri() ?>/images/logo.png" alt="">
                            <p>Paga Subito Online Così Ritirerai il Pacco Senza Dover Dare Soldi al Postino</p>
                            <?php if (isset($payment_block_items)): ?>
                                <?= $payment_block_items; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php

get_footer();
