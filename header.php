<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 14:53
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <link rel="shortcut icon" href="<?= get_template_directory_uri() ?>/images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <title><?= get_the_title(); ?></title>

</head>
<body <?= is_page(173) ? "class='cat'" : '' ?>>
<?php wp_head(); ?>
