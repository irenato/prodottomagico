<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 05.12.16
 * Time: 14:41
 */

/**
 * Template name: Magica Notte
 */

get_header();
$form_top = get_field('contact_form_7_top');
$form_bottom = get_field('contact_form_7_bottom');
$advantages = get_field('advantages');
$image1 = get_field('background_1');
$image2 = get_field('background_2');
$image3 = get_field('background_3');

?>

    <div id='page-preloader' data-page="<?= get_the_ID() ?>"
         data-product="<?= get_post_field('post_name', get_the_ID()) ?>" style='display: none'><span
            class='spinner'></span></div>

    <!--START CONTENT-->

    <section>
        <div class="bg-block magica-notte" style="background-image: url(<?= $image1 ?>)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block">
                            <h1 class="title">Ordina Subito Clip Magica Notte</h1>
                            <p class="text">L'unica Clip Anti-Russamento che Ti Regala Sonno di Qualità Gia dalla Prima
                                Notte</p>
                            <div class="forma forma1">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab2" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <?= do_shortcode($form_top) ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="block-2 magica-notte">

                        <?php foreach ($advantages as $advantage): ?>
                            <div class="item">
                                <div class="img">
                                    <img src="<?= $advantage['image'] ?>" alt="">
                                </div>
                                <h4 class="title"><?= $advantage['title'] ?></h4>
                                <?= $advantage['description'] ?>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="bg-block-3 magica-notte" style="background-image: url(<?= $image2 ?>)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block-3">
                            <h2 class="title">acquista la Clip Magica Notte ad un super prezzo!</h2>
                            <div class="description">
                                <p><span>Regala a te e al tuo partner tutti i benefici di un sonno più tranquillo, grazie a questa rivoluzionaria clip che aiuta a smettere di russare.</span>
                                </p>
                                <p><span>Come funziona:</span> I due magneti, inclusi nel prezzo della clip, hanno la
                                    funzione di facilitare l'apertura del setto nasale facilitando la respirazione ed
                                    eliminando ogni russamento. Per godere istantaneamente dei benefici della clip e
                                    regalarti il sonno che hai sempre desiderato, ti basterà indossare la clip nasale
                                    prima di andare a letto.</p>
                                <p>Acquista subito l'invenzione che sta facendo impazzire gli USA e che <b>ora è
                                        disponibile anche in Italia</b> per un tempo limitato!*</p>
                                <p class="ttu"><b>Acquistando oggi riceverai Clip Magica Notte® a SOLI 24.90€ invece che
                                        49€!</b></p>
                                <p class="mini">*Non troverai Clip Magica Notte in nessun negozio fisico in Italia. Puoi
                                    acquistare la clip esclusivamente online e solo fino ad esaurimento scorte.</p>
                                <a href="#scrollform-bottom" class="button">Ordina Ora</a>
                                <img src="<?= get_template_directory_uri() ?>/images/img-4.png" alt="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="block-4 magica-notte">
                        <div class="reviews">

                            <?php $connected = new WP_Query(array(
                                'connected_type' => 'products_to_reviews',
                                'connected_items' => get_queried_object(),
                                'nopaging' => true,
                            ));
                            ?>
                            <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                <div class="reviews-item hide-mobile">
                                    <div class="photo"
                                         style="background-image: url(<?= get_the_post_thumbnail_url() ?>)"></div>
                                    <h3 class="title"><?= get_the_title() ?></h3>
                                    <p><?= get_the_content() ?></p>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                            <div class="reviews-slider">
                                <?php $connected = new WP_Query(array(
                                    'connected_type' => 'products_to_reviews',
                                    'connected_items' => get_queried_object(),
                                    'nopaging' => true,
                                ));
                                ?>
                                <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                    <div class="reviews-item">
                                        <div class="photo"
                                             style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                                        <h3 class="title"><?= get_the_title() ?></h3>
                                        <p><?= get_the_content() ?></p>
                                    </div>
                                <?php endwhile; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-block-5 magica-notte"
             style="background-image: url(<?= $image3 ?>); background-repeat: no-repeat !important;"
             id="scrollform-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block-5">
                            <div class="forma bottom">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab3" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab4" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?= do_shortcode($form_bottom) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END CONTENT-->
    <div class="dark-side-of-moon" style="display: none"></div>

<?php
get_footer();