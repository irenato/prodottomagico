<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 17:31
 */

/**
 * Template name: Aspirapeli Magico
 */
get_header();

$form_top = get_field('contact_form_7_top');
$form_bottom = get_field('contact_form_7_bottom');
$image1 = get_field('background_1');
$image2 = get_field('background_2');
$image3 = get_field('background_3');
$vacuum_1 = get_field('vacuum_1');
$vacuum_2 = get_field('vacuum_2');
$advantages = get_field('advantages');
$price_block = get_field('price_block');

?>
    <div id='page-preloader' data-page="<?= get_the_ID() ?>"
         data-product="<?= get_post_field('post_name', get_the_ID()) ?>" style='display: none'><span
            class='spinner'></span></div>
    <!--START CONTENT-->
    <section class="block" style="background:
    <?php if ($vacuum_1): ?>
        url(<?= $vacuum_1 ?>) no-repeat bottom right,
    <?php endif; ?>
        url(<?= $image1 ?>);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= get_the_content(); ?>
                    <div class="forma forma1">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab" data-toggle="tab">Passo 1</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab" class="next-step">Passo 2</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <?= do_shortcode($form_top) ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="video-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="video">
                        <!--					<img src="images/video.png" alt="">-->
                        <iframe src="<?= get_field('youtube_link') ?>" frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                    <?= $price_block ?>
                    <a href="#scrollform-bottom" class="button">ORDINA ADESSO</a>
                </div>
            </div>
        </div>
    </section>
    <section class="block-3">
        <?php if (is_page(173)): ?>
            <img src="<?= $image2 ?>" alt="">
        <?php endif; ?>
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if (is_page(8)): ?>
                        <img src="<?= $image2 ?>" alt="">
                    <?php endif; ?>
                    <!--                    <img src="-->
                    <?//= get_template_directory_uri() ?><!--/images/dog-images.png" alt="">-->
                    <div class="text">
                        <?php if ($advantages): ?>
                            <?php foreach ($advantages as $advantage): ?>
                                <h4 class="title"><?= $advantage['title'] ?></h4>
                                <p><?= $advantage['description'] ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="block-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="reviews">

                        <?php $connected = new WP_Query(array(
                            'connected_type' => 'products_to_reviews',
                            'connected_items' => get_queried_object(),
                            'nopaging' => true,
                        ));
                        ?>
                        <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                            <div class="reviews-item hide-mobile">
                                <div class="photo"
                                     style="background-image: url(<?= get_the_post_thumbnail_url() ?>)"></div>
                                <h3 class="title"><?= get_the_title() ?></h3>
                                <p><?= get_the_content() ?></p>
                            </div>
                        <?php endwhile; ?>
                        <?php endif; ?>

                        <div class="reviews-slider">
                            <?php $connected = new WP_Query(array(
                                'connected_type' => 'products_to_reviews',
                                'connected_items' => get_queried_object(),
                                'nopaging' => true,
                            ));
                            ?>
                            <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                <div class="reviews-item">
                                    <div class="photo"
                                         style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                                    <h3 class="title"><?= get_the_title() ?></h3>
                                    <p><?= get_the_content() ?></p>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="block-5" style="background-color: #f5f5f5;" id="scrollform-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="<?= $image3 ?>" alt="">
                    <!--                    <img src="-->
                    <?//= get_template_directory_uri() ?><!--/images/dog-bg.png" alt="">-->
                    <div class="forma bottom">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab3" data-toggle="tab">Passo 1</a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab" class="next-step">Passo 2</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <?= do_shortcode($form_bottom) ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END CONTENT-->
    <div class="dark-side-of-moon" style="display: none"></div>

<?php
get_footer();
