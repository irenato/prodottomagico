<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 14:53
 */

include ('settings/scripts_and_styles.php');
include ('settings/simple_costum_posts.php');
include ('settings/ajax_functions.php');
include ('settings/active_compaign.php');
include ('settings/action.php');
//include ('settings/menu_items.php');

add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('customize_register', 'themeslug_theme_customizer');

//add_filter( 'wpcf7_validate_configuration', '__return_false' );