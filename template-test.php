<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 17:31
 */

/**
 * Template name: Test
 */
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
if (isset($_GET['fbid']) && isset($_GET['aff_id'])) {
    header('Location: http:' . $_SERVER['HTTP_HOST'] . $uri_parts[0] . '/confirmation?fbid=' . $_GET['fbid'] . 'aff_id=' . $_GET['aff_id']);
}
get_header();

?>


                        <!-- Tab panes -->
                        <div class="tab-content">

                            <?= do_shortcode('[contact-form-7 id="19" title="Aspirapeli Magico Top"]') ?>

                        </div>


<?php
get_footer();
