<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 11.11.16
 * Time: 14:53
 */
?>

<!--START FOOTER-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="copyright">
                    <p>Copyright 2016 - Offerte Prodotto Magico - All Rights Reserved
                    <?php if (is_page(8)): ?>
                        <span>Nota bene: Il prodotto è adatto per cani a pelo corto e ogni cane reagisce in maniera
                            differente al rumore della ventola</span>
                    <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var trackcmp_email = '';
        var trackcmp = document.createElement("script");
        trackcmp.async = true;
        trackcmp.type = 'text/javascript';
        trackcmp.src = '//trackcmp.net/visit?actid=89111504&e=' + encodeURIComponent(trackcmp_email) + '&r=' + encodeURIComponent(document.referrer) + '&u=' + encodeURIComponent(window.location.href);
        var trackcmp_s = document.getElementsByTagName("script");
        if (trackcmp_s.length) {
            trackcmp_s[0].parentNode.appendChild(trackcmp);
        } else {
            var trackcmp_h = document.getElementsByTagName("head");
            trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
        }
    </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73252401-8', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/376fcf21-98f2-4e5b-b698-678304179f0f.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>
</footer>


<!--END FOOTER-->
<?php wp_footer(); ?>
</body>

</html>
