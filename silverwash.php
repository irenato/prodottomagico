<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 08.12.16
 * Time: 17:29
 */

/**
 * Template name: Silver wash
 */

get_header();

$links = use_products_links(get_the_ID());
$aff_id = isset($_GET['aff_id']) ? '&aff_id=' . $_GET['aff_id'] : '';
$fbid = isset($_GET['fbid']) ? '&fbid=' . $_GET['fbid'] : '';

?>

<!--START CONTENT-->
<section class="silver-wash">
    <div class="bg-block" style="background: url(<?= get_template_directory_uri() ?>/images/background.png)no-repeat bottom right;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content">
                        <h1 class="title">SCOPRI SILVER WASH!</h1>
                        <h5 class="title">Mai più batteri nella tua biancheria!</h5>
                        <p class="text">Ti presentiamo SILVER WASH <br> L'unico panno con ioni d'argento che elimina qualsiasi batterio dai tuoi   capi. Lavaggi senza ammorbidente e con solo 1/4 di detersivo per un risparmio incredibile.</p>
                        <div class="hide">
							<h5 class="title"><b>Ottieni Subito il Tuo Silver Wash Adesso Solo Per    29,95 - Offerta Limitata!</b></h5>
							<p class="ordina">Ordina ora a soli <i>49,99  <sap>&#8364;</sap></i> - <span>29,95 <sap>&#8364;</sap> + Spedizione</span></p>
							<a href="<?= $links[0]['url'] . $aff_id . $fbid ?>" class="button">Acquista 1 Silver Wash</a>
							<h4 class="title">ASPETTA</h4>
							<p class="text">aggiungi un panno con <span>4,99 <sap>&#8364;</sap></span> di sconto</p>
							<a href="<?= $links[1]['url'] . $aff_id . $fbid  ?>" class="button">Acquista 2 Silver Wash</a>
                        </div>
                        <img src="<?= get_template_directory_uri() ?>/images/silver-wash.png" alt="" class="silver-wash">
                        <a href="#scrollform-bottom" class="button">Ordina Adesso</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="why-we">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <h2 class="title">Perché usare Silver Wash?</h2>
                    <div class="items">
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/nose.png" alt="">
                            <div class="description">
                                <h5 class="title">Addio Cattivi odori</h5>
                                <p class="text">Potrai dimenticare senza preoccupazione i tuoi capi nella lavatrice senza avere il timore che prendano quel cattivo odore che sicuramente conosci</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/home.png" alt="">
                            <div class="description">
                                <h5 class="title">Una casa Igienizzata </h5>
                                <p class="text">Silver Wash non renderà puliti solo i tuoi capi ma anche i tessuti con cui verranno a contatto i capi che indossi; come ad esempio il letto e il divano</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/cleaning.png" alt="">
                            <div class="description">
                                <h5 class="title">Risparmio garantito</h5>
                                <p class="text">Con Silver Wash non dovrai più usare l'ammorbidente e ti basterà solo 1/4 del detersivo che usi normalmente. Il risparmio è chiaro!</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/security.png" alt="">
                            <div class="description">
                                <h5 class="title">Protezione</h5>
                                <p class="text">Protegge da funghi e batteri ed allevia le malattie della pelle come la dermatite. I capi indossati avranno una maggiore resistenza agli odori</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ecco">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="description">
                        <h2 class="title">Ecco come funzionano le particelle di ARGENTO</h2>
                        <h5 class="title">L'ARGENTO (Ag+)</h5>
                        <p class="text">La ricerca biomedica ha dimostrato che nessun organismo conosciuto fra batteri, virus, alghe, funghi e muffe, puòvivere piùdi qualche minuto in presenza di una traccia di argento metallico. Il rilascio di ioni argento Ag+ agisce ad un livello intracellulare, interrompendo il DNA dei microrganismi presenti sul tessuto e soffocandone efficacemente l’attività. Inoltre i ceppi resistenti non riescono a svilupparsi quando viene usato l’argento.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="why">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="item">
                        <img src="<?= get_template_directory_uri() ?>/images/image-1.png" alt="">
                        <div class="description">
                            <h3 class="title">FUNGHI</h3>
                            <p class="text">SilverWash elimina funghi, muffe e spore. Quando si riduce la naturale funzione di protezione della pelle, in presenza di micosi, del piede dell'atleta, di infezioni vaginali, ecc. c'è sempre il rischio di acutizzare il problema e trasmetterlo ad altre persone</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?= get_template_directory_uri() ?>/images/image-2.png" alt="">
                        <div class="description">
                            <h3 class="title">BATTERI</h3>
                            <p class="text">SilverWash® elimina i batteri che, altrimenti, con l'umidità del corpo, prolificano. Gli scarti dei batteri solo una delle principali cause dei cattivi odori e delle irritazioni cutanee. L'argento è utile per debellare le infezioni di MRSA e legionella.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?= get_template_directory_uri() ?>/images/image-3.png" alt="">
                        <div class="description">
                            <h3 class="title">VIRUS</h3>
                            <p class="text">SilverWash elimina i virus. Il rilascio di ioni d’argento Ag+ agisce ad un livello intracellulare interrompendo il DNA, soffocando efficacemente l’attività dei microrganismi presenti sui tessuti. L’effetto igienizzante degli ioni Ag+ dura dai  3 ai 7 giorni.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?= get_template_directory_uri() ?>/images/image-4.png" alt="">
                        <div class="description">
                            <h3 class="title">ACARI</h3>
                            <p class="text">SilverWash evita la crescita di un fungo (Aspergillus repens) che cresce sulle cellule morte della pelle che perdiamo sulle lenzuola. Gli acari si nutrono di questo fungo. Interrompendo la catena alimentare gli acari spariscono</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="video-silver-wash">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="title">COME SI USA</h2>
                    <p class="text">Ecco la recensione fatta da un Cliente</p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/KZpAdkOhuFg" frameborder="0" allowfullscreen></iframe>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/CuCeYTw0A3M" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="buy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="items">
                        <div class="item">
                            <div class="header">
                                <p><b>1 PANNO</b></p>
                                <h2 class="title"><sap>&#8364;</sap> 29,95</h2>
                                <p>+ spedizione</p>
                            </div>
                            <div class="bottom">
                                <p>1 panno Silver Wash</p>
                                <p>150 lavaggi garantiti</p>
                                <a href="<?= $links[0]['url'] . $aff_id . $fbid  ?>" class="button">Acquista Ora</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="header">
                                <p><b>2 PANNI</b></p>
                                <h2 class="title"><sap>&#8364;</sap> 50</h2>
                                <p>+ spedizione</p>
                            </div>
                            <div class="bottom">
                                <p>2 panni Silver Wash</p>
                                <p>300 lavaggi garantiti</p>
                                <p>risparmi il 20%</p>
                                <a href="<?= $links[1]['url'] . $aff_id . $fbid  ?>" class="button">Acquista Ora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="reviews">

                        <?php $connected = new WP_Query(array(
                            'connected_type' => 'products_to_reviews',
                            'connected_items' => get_queried_object(),
                            'nopaging' => true,
                        ));
                        ?>
                        <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                            <div class="reviews-item hide-mobile">
                                <div class="photo"
                                     style="background-image: url(<?= get_the_post_thumbnail_url() ?>)"></div>
                                <h3 class="title"><?= get_the_title() ?></h3>
                                <p><?= get_the_content() ?></p>
                            </div>
                        <?php endwhile; ?>
                        <?php endif; ?>

                        <div class="reviews-slider">
                            <?php $connected = new WP_Query(array(
                                'connected_type' => 'products_to_reviews',
                                'connected_items' => get_queried_object(),
                                'nopaging' => true,
                            ));
                            ?>
                            <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                <div class="reviews-item">
                                    <div class="photo"
                                         style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                                    <h3 class="title"><?= get_the_title() ?></h3>
                                    <p><?= get_the_content() ?></p>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="description-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="title">LE DOMANDE FREQUENTI</h2>
                    <ul>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Quanto dura il panno?</b> <span>il panno è garantito fino a 150 lavaggi</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>E' un prodotto originale?</b> <span>Silver Wash è un prodotto originale e a marchio registrato da CALVI tecnologie srl</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Posso usarlo anche in acqua fredda?</b> <span>Certamente, consigliamo di usare il panno in acqua fredda o a 30 gradi, risparmiando così anche sul consumo di elettricità</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Posso usarlo con tutti i capi?</b> <span>Silver Wash può essere usato con qualsiasi tipo di tessuto</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Come capisco quando l'efficacia è terminata?</b> <span>Non è presente un'indicatore specifico, tuttavia si capisce che è da cambiare quando si ricomincia a sentire il classico odore sgradevole lasciando i panno qualche minuto in lavatrice. Inoltre lo si percepisce dalla biancheria che inizierà a non esse più morbida come prima.</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Quali vantaggi pratici mi comporta l'utilizzo del panno?</b> <span>Riassumendo brevemente: Igiene totale - risparmio sull'elettricità - risparmio sui detersivi - possibilità di indossare gli indumenti più a lungo senza che puzzino - protezione igienica dell'ambiente a contatto con i tessuti trattati</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Dopo l'utilizzo come ripongo il panno?</b> <span>Una volta terminato il lavaggio consigliamo di stendere il panno insieme alla biancheria, questo aumenterà la sua efficacia e servirà per farlo asciugare. Una volta che il panno è asciutto potete riporlo nel cestello della lavatrice in attesa di un nuovo lavaggio.</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Lo trovo anche nei supermercati?</b> <span>Purtroppo no! Il Silver Wash è stato rifiutato dalla grande distribuzione a causa della sua lunga durata ( 150 giorni ) la quale non porta i clienti al riacquisto frequente. E' stato rifiutato quindi solo per interessi economici delle grandi catene.</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Avete detto di non usare l'ammorbidente è corretto?</b> <span>Esattamente! L'ammorbidente è la più grande "fregatura" che ci sia! Infatti esso sembra che profumi e renda i capi morbidi, in realtà non fa altro che ricoprire il tessuto di un "velo" nocivo, il quale viene assorbito dalla pelle quando avete i vestiti addosso o dormite nel letto con le lenzuola trattate con ammorbidente</span></p>
                        </li>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/images/question-answer.png" alt="">
                            <p><b>Funziona come acchiappacolore?</b> <span>Questa versione di Silver Wash non ha la funzione di "acchiappacolore". E' in progettazione un nuovo modello che avrà anche questa caratteristica</span></p>
                        </li>
                    </ul>
                    <div class="bottom-description" id="scrollform-bottom">
                        <h3 class="title">Ottieni Subito il Tuo Silver Wash Adesso Solo Per <sap>&#8364;</sap> 29,95 - Offerta Limitata!</h3>
                        <p class="ordina">Ordina ora a soli <i>49,99 <sap>&#8364;</sap></i> - <span>29,95 <sap>&#8364;</sap> + Spedizione</span></p>
                        <a href="<?= $links[0]['url'] . $aff_id . $fbid  ?>" class="button">Acquista 1 Silver Wash</a>
                        <h3 class="title">ASPETTA</h3>
                        <p>aggiungi un panno con <span>4,99 <sap>&#8364;</sap></span> di sconto</p>
                        <a href="<?= $links[1]['url'] . $aff_id . $fbid  ?>" class="button">Acquista 2 Silver Wash</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php

get_footer();
