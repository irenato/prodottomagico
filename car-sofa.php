<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 13:06
 */

/**
 * Template name: Car Sofa
 */

get_header();

$form_top = get_field('contact_form_7_top');
$form_bottom = get_field('contact_form_7_bottom');
$products = get_field('product_block');

?>
    <div id='page-preloader' data-page="<?= get_the_ID() ?>"
         data-product="<?= get_post_field('post_name', get_the_ID()) ?>" style='display: none'><span
            class='spinner'></span></div>
    <!--START CONTENT-->
    <section class="car-sofa">
        <div class="bg-block"
             style="background-image:url(<?= get_template_directory_uri() ?>/images/sofa.png), url(<?= get_template_directory_uri() ?>/images/car-sofa-bg.png);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block">
                            <h1 class="title">Car Sofa, il tuo Sofa per auto automatico, pronto in 40 secondi!</h1>
                            <div class="forma forma1">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab2" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <?= do_shortcode($form_top) ?>

                                </div>
                            </div>
                            <a href="#scrollform-bottom" class="button mobile">Ordina Adesso</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="why-we">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="items">
                            <div class="item">
                                <img src="<?= get_template_directory_uri() ?>/images/alta-qualita.png" alt="">
                                <div class="description">
                                    <p class="text">Pelle scamosciata <br> alta qualità</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="<?= get_template_directory_uri() ?>/images/compressore.png" alt="">
                                <div class="description">
                                    <p class="text">Compressore da <br> accendisigari in omaggio</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="<?= get_template_directory_uri() ?>/images/car.png" alt="">
                                <div class="description">
                                    <p class="text">Compatibile con il <br> 97% delle auto</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="<?= get_template_directory_uri() ?>/images/delivery.png" alt="">
                                <div class="description">
                                    <p class="text">Spedizione omaggio <br> e paghi alla consegna</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="transforma">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="title">Trasforma la tua auto in una suite di lusso</h2>
                        <p class="text">L’unico ITALIANO, l’unico ORIGINALE, soddisfatto o rimborsato 2 volte! <br>
                            CarSofa è il tuo letto per auto ideale per le tue gite fuori porta, per i tuoi lunghi
                            viaggi, per <br> il relax dei tuoi bimbi o per il relax tuo o del tuo partner!</p>
                        <div class="item">
                            <h3 class="title">AUTOMATICO</h3>
                            <p class="text">Gonfia il tuo CarSofa in 40 secondi senza fatica, collega il compressore
                                all’accendisigari della tua auto ed in meno di un minuto il gioco è fatto</p>
                        </div>
                        <div class="item">
                            <h3 class="title">PRATICO</h3>
                            <p class="text">In pochissimi secondi trasformerai i sedili posteriori della tua auto in una
                                comodissima suite, grazie alla sua forma “ad L” avrai una maggiore stabilità</p>
                        </div>
                        <div class="item">
                            <h3 class="title">INDISPENSABILE</h3>
                            <p class="text">Grazie alle sue misure standard sarà adattabile a quasi tutte le auto. Oltre
                                alla pompa automatica avrai inclusi due comodi cuscini per un relax totale</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="caratteristiche">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
                            <h2 class="title">caratteristiche</h2>
                            <p class="text">Le misure standard di CarSofa (135cm x 88cm x 45cm) permettono di <br>
                                adattarsi in pratica a tutte le auto.</p>
                            <p class="text">Il rivestimento superiore di pelle scamosciata ti permetterà di avere un
                                <br> materasso matrimoniale completamente stabile ed ancorato alla tua auto.</p>
                            <p class="text">Mai più riposi scomodi, mai più paura dei colpi di sonno durante i tuoi
                                viaggi. <br> Da oggi la tua auto diventerà il tuo resort a 5 stelle.</p>
                            <h5 class="title">Non fidarti da chi vende e spedisce dall’estero <br> (Amazon, ecc).</h5>
                            <p class="text">Alla fine ti ritroverai a pagare 130/140 Euro per un prodotto copia, perchè
                                da <br> loro, alla consegna, pagherai anche IVA e dogana, inoltre ti faranno aspettare
                                2/3 settimane. Acquista qui, spediremo dall’Italia e riceverai il tuo CarSofa in 2
                                GIORNI. Da noi la spedizione è RAPIDA, dall'Italia, e siamo i soli che ti permettono di pagare direttamente alla CONSEGNA! Nessun centesimo in più, promesso!</p>
                            <h5 class="title">Compressore ad accendisigari INCLUSO</h5>
                            <p class="text"><span>Diffida dalle cineserie che non si gonfiano o si sgonfiano dopo 2 giorni. <br> Solo CarSofa ti offre la garanzia soddifatti o rimborsati due volte!</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="buy">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="title">Scegli il tuo colore qui e clicca su ACQUISTA!</h2>
                        <div class="items">
                            <?php foreach ($products as $product) : ?>
                                <div class="item">
                                    <div class="header">
                                        <img src="<?= $product['image'] ?>" alt="">
                                        <p class="sconto">Sconto <?= $product['discount'] ?>%</p>
                                    </div>
                                    <div class="bottom">
                                        <h3 class="title"><?= $product['title'] ?></h3>
                                        <h2 class="title"><span>&#8364;</span><?= $product['price'] ?></h2>
                                        <p class="text"><?= $product['description'] ?></p>
<!--                                        <a href="// $product['link'] . '?colour=' . $product['colour'] " class="button">ACQUISTA</a>-->
                                         <a href="#scrollform-bottom" class="button">ACQUISTA</a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="transforma descripton">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="title">1.700 ORDINI NEL MESE DI
                            OTTOBRE<span>AFFRETTATI, HAI UNO SCONTO DEL 31%</span></h2>
                        <p class="text">SOLO PER POCHISSIMO TEMPO, oltre a questo SCONTO, se acquisti 2 o più CarSofa
                            avrai dal 20% al 30% di sconto ULTERIORE su ogni pezzo! <b>In più avrai in omaggio:</b></p>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/Due-cuscini.png" alt="">
                            <p class="text"><b>Due cuscini</b></p>
                        </div>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/compressore.png" alt="">
                            <p class="text"><b>Compressore <br> ad accendisigari</b></p>
                        </div>
                        <div class="item">
                            <img src="<?= get_template_directory_uri() ?>/images/Kit-di-riparazione.png" alt="">
                            <p class="text"><b>Kit di riparazione</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="video-silver-wash">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="title">Non sei ancora convinto? Guarda qui</h2>
                        <p class="text">Guarda il nostro video e scopri come è semplice e pratico gonfiare CarSofa. Il
                            massimo del relax nella tua auto in 40 secondi!</p>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/xE15MjPf-rY" frameborder="0"
                                allowfullscreen></iframe>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/scQc4Ti7Brc" frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>


        <div class="block-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="reviews">
                            <?php $connected = new WP_Query(array(
                                'connected_type' => 'products_to_reviews',
                                'connected_items' => get_queried_object(),
                                'nopaging' => true,
                            ));
                            ?>
                            <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                <div class="reviews-item hide-mobile">
                                    <div class="photo"
                                         style="background-image: url(<?= get_the_post_thumbnail_url() ?>)"></div>
                                    <h3 class="title"><?= get_the_title() ?></h3>
                                    <p><?= get_the_content() ?></p>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                            <div class="reviews-slider">
                                <?php $connected = new WP_Query(array(
                                    'connected_type' => 'products_to_reviews',
                                    'connected_items' => get_queried_object(),
                                    'nopaging' => true,
                                ));
                                ?>
                                <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                    <div class="reviews-item">
                                        <div class="photo"
                                             style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                                        <h3 class="title"><?= get_the_title() ?></h3>
                                        <p><?= get_the_content() ?></p>
                                    </div>
                                <?php endwhile; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-block-5 magica-notte" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <h2 class="title form-bottom">Ordina Ora e Paga alla Consegna</h2>
                        <div class="block-5">
                           
                            <div class="forma bottom" id="scrollform-bottom">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab3" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab4" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?= do_shortcode($form_bottom) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--END CONTENT-->

    <div class="dark-side-of-moon" style="display: none"></div>

<?php
get_footer();
