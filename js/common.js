// var $ = jQuery;

jQuery(document).ready(function ($) {

    $('a[href*=#scroll]:not([href=#scroll])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $(document).on('click', 'button.disabled-click', function () {
        $(this).closest('.col-lg-12').find('a.next-step').trigger('click');
        return false;
    })

    var current_url = window.location.href,
        filtered_url = current_url.substring(0, current_url.indexOf('?')),
        before_get_param = current_url.split('?'),
        get_param = '';

// $(document).ready(function () {
    $('.reviews-slider').slick({adaptiveHeight: true});

    $("#numberBottomForm, #number").focus(function () {
        var str = '+39?(000) 000-00-00';
        $(this).mask(str);
    });

    $("input[name='phone']").mask('+39?(000) 000-00-00');

    function nameValid(that) {


        var rv_name = /([a-zA-Z]+(\W+)?){2,}(\s+)?/;
        if ($(that).val().replace(/\W+/g, '').length > 2 && $(that).val() !== '' && rv_name.test($(that).val())) {
            $(that).removeClass('error').addClass('success');
            $(that).closest('span').next().next().hide();
        } else {
            $(that).removeClass('success').addClass('error');
            $(that).closest('div').find('p.error-message').show();
        }
    }

    function notBeEmpty(that) {
        if ($(that).val().length != 0) {
            $(that).removeClass('error').addClass('success');
            $(that).closest('span').next().next().hide();
        } else {
            $(that).removeClass('success').addClass('error');
            $(that).closest('div').find('p.error-message').show();
        }
    }

    function checkValLength(that, len) {
        if ($(that).val().length > len) {
            $(that).removeClass('error').addClass('success');
            $(that).closest('span').next().next().hide();
        } else {
            $(that).removeClass('success').addClass('error');
            $(that).closest('div').find('p.error-message').show();
        }
    }

    function number(that) {

//        var rv_name = /^\+39\([0-9]{3}\) [0-9]{3}\-[0-9]{2}\-[0-9]{1,2}?$/;
		var rv_name = /^\+39\([0-9]{3}\) [0-9]{3}\-[0-9]{2}\-[0-9]{1,2}?_?$/;

        if (rv_name.test($(that).val())) {
            $(that).removeClass('error').addClass('success');
        } else {
            $(that).removeClass('success').addClass('error');
        }
    }

    function emailValid(that) {
        var rv_mail = /.+@.+\..+/i;

        if ($(that).val() !== '' && rv_mail.test($(that).val())) {
            $(that).removeClass('error').addClass('success');
            $(that).closest('span').next().next().hide();
        } else {
            $(that).removeClass('success').addClass('error');
            $(that).closest('div').find('p.error-message').show();
        }
    }

    $(document).on('click', '.first-step', function () {
        // e.preventDefault();
        var current_form = $(this).closest('form');
        $(current_form).find('input').each(function () {
            var name = $(this).attr('name'),
                val = $(this).val();

            switch (name) {
                case 'first-name':
                    nameValid(this);
                    break;

                case 'last-name':
                    nameValid(this);
                    break;

                case 'phone':
                    number(this);
                    break;


                case 'email':
                    emailValid(this);
                    break;


            } // end switch(...)

        });
        if ($(current_form).find('.error').length < 1) {
            var first_name = $(current_form).find('input[name=first-name]').val(),
                last_name = $(current_form).find('input[name=last-name]').val(),
                phone = $(current_form).find('input[name=phone]').val(),
                email = $(current_form).find('input[name=email]').val();
            $(current_form).closest('.forma').find('a.next-step').trigger('click');
            $('.forma .nav li').addClass('active');
            $('.forma .nav li:first-child').removeClass('active');
            if ($(current_form).find('input[name=email]').hasClass('success'))
                sendContact(email, first_name, last_name, phone, $(this));
            showErrors($(current_form));
            $(this).removeClass('first-step').addClass('disabled-click');
            return false;
        } else {
            showErrors($(current_form));
            return false;
        }
    });

    $(document).on('keyup', "input[name='phone']", function () {
        number(this);
    });

//    $("input[name='post_code'], input[name='address'], input[name='city']").bind('textchange', function (event, previousText) {
//        checkValLength(this, 2);
//    });

    // $("input[name='first-name'], input[name='last-name']").bind('keyup', function () {
    //     nameValid(this);
    // });
    $("input[name='first-name'], input[name='last-name']").bind('textchange', function (event, previousText) {
        nameValid(this);
    });

    $("input[name='email']").bind('textchange', function (event, previousText) {
        emailValid(this);
    });

    // $("input[name='house_number'], input[name='last-name']").bind('textchange', function (event, previousText) {
    //     checkValLength(this, 0);
    // });


    $("input[name='house_number']").bind('textchange', function (event, previousText) {
        checkValLength(this, 0);
    });




    $("input[name='region']").bind('textchange', function (event, previousText) {
        checkValLength(this, 1);
    });

    $(document).on('click', '.wpcf7-submit', function () {
        // e.preventDefault();
        var current_form = $(this).closest('form');
        // notBeEmpty($(current_form).find('textarea'));
        $(current_form).find('input').each(function () {
            var name = $(this).attr('name'),
                val = $(this).val();

            switch (name) {
                case 'first-name':
                    nameValid(this);
                    break;

                case 'last-name':
                    nameValid(this);
                    break;

                case 'phone':
                    number(this);
                    break;

                case 'email':
                    emailValid(this);
                    break;

                case 'address':
                    checkValLength(this, 2);
                    break;

                case 'house_number':
                    checkValLength(this, 0);
                    break;

                case 'city':
                    checkValLength(this, 2);
                    break;

                case 'region':
                    checkValLength(this, 1);
                    break;


                case 'post_code':
                    checkValLength(this, 2);
                    break;

            } // end switch(...)

        });
        if ($(current_form).find('.error').length < 1) {
            sendMailForTags($(current_form).find('input[name=email]').val());
            // }
            $(current_form).closest('.forma').find('a.next-step').trigger('click');
            get_param = $(current_form).find('select').val()[0];
            showErrors($(current_form));
            $('#page-preloader').show();
            $('.dark-side-of-moon').show();
            $('.wpcf7-submit').hide();
            return true;
        } else {
            showErrors($(current_form));
            return false;
        }
    });

    function showErrors(current_form) {
        var errors_parent = current_form.find('.error').closest('div').find('p.error-messages').show(),
            success_parent = current_form.find('.success').closest('div').find('p.error-messages').hide();
        if ($(window).width() < 991) {
            if (errors_parent.length > 0) {
                var elementClick = $(errors_parent)[0].closest('div');
                var destination = $(elementClick).offset().top;
                if ($.browser.safari) {
                    $('body').animate({scrollTop: destination}, 1100); //1100 - скорость
                } else {
                    $('html').animate({scrollTop: destination}, 1100);
                }
            }
        }
    }

    function sendMailForTags(email) {
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'addTag',
                'page': $('#page-preloader').attr('data-page'),
                'email': email
            },
        });
    }

    function sendContact(email, first_name, last_name, phone, input) {
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'addContact',
                'email': email,
                'first_name': first_name,
                'last_name': last_name,
                'phone': phone,
                'page': $('#page-preloader').attr('data-page')
            },
            success: $(input).removeClass('first-step').addClass('disabled-click'),
        });
    }

    $(document).on('mailsent.wpcf7', function () {

        $('#page-preloader').hide();
        $('.dark-side-of-moon').hide();


        var boolPhone = $(this).find('input[name=phone]').hasClass('error');

        var getParam = (boolPhone) ? 0 : 1;


        if (get_param == 1)
            $(location).attr('href', 'https://offerte.prodottomagico.com/ordine-aspirapeli-magico-conferma/?' + (typeof before_get_param[1] != 'undefined' ? before_get_param[1] + '&' : '') + 'type=' + get_param);
        else
            $(location).attr('href', 'https://offerte.prodottomagico.com/ordine-aspirapeli-magico-conferma-x2/?' + (typeof before_get_param[1] != 'undefined' ? before_get_param[1] + '&' : '') + 'type=' + get_param);
        // $(location).attr('href', 'https://ordina.prodottomagico.com/confirmation?' + (typeof before_get_param[1] != 'undefined' ? before_get_param[1] + '&' : '') + 'prod=' + $('#page-preloader').attr('data-product') + '&type=' + get_param + '&param=' + $('#page-preloader').attr('data-page'));
    });


    // });
});