<?php
 /*
 * Template name: SMS Link Confirmation
 */
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
if (isset($_GET['fbid']) && isset($_GET['aff_id'])) {
    header('Location: http:' . $_SERVER['HTTP_HOST'] . $uri_parts[0] . '/confirmation?fbid=' . $_GET['fbid'] . 'aff_id=' . $_GET['aff_id']);
}
get_header();

?>

<div class="wrapper"> 
<h2 class="title" ><?php the_content(); ?></h2>
</div>
<style>
html, body {height: 100%; margin: 0; padding: 0;}

.wrapper {
    min-height: 100%;
    margin: -116px auto 0;
}
.title{text-align: center;padding-top: 20%;}
@media screen and (max-width: 480px) {
    .title{text-align: center;padding-top: 60%;}
    .wrapper {
        min-height: 100%;
        margin: -133px auto 0;
    }
}
@media screen and (min-width: 481px) and (max-width: 981px) {
    .title{text-align: center;padding-top: 50%;}
    .wrapper {
        min-height: 100%;
        margin: -125px auto 0;
    }
}
</style>

<?php
get_footer();
