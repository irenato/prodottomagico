<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 13.12.16
 * Time: 15:52
 */

/**
 * Template name: Ordina Lettino
 */

get_header();
$form_top = get_field('contact_form_7_top');
$form_bottom = get_field('contact_form_7_bottom');
$advantages = get_field('advantages');
$image1 = get_field('background_1');
$image2 = get_field('background_2');
$image3 = get_field('background_3');
?>

    <div id='page-preloader' data-page="<?= get_the_ID() ?>"
         data-product="<?= get_post_field('post_name', get_the_ID()) ?>" style='display: none'><span
            class='spinner'></span></div>

    <!--START CONTENT-->

    <section class="lettino">
        <div class="bg-block" style="background-image: url(<?= $image1 ?>)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block">
                            <h1 class="title">ORDINA LETTINO MAGICO,E FAI FELICE IL TUO GATTO</h1>
                            <p class="text">Ordina adesso online, paghi direttamente alla consegna del prodotto</p>
                            <div class="forma forma1">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab2" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <?= do_shortcode($form_top) ?>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <img src="<?= $image1 ?>" alt="" class="img-mobile">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="block-2 magica-notte">

                        <?php foreach ($advantages as $advantage): ?>
                            <div class="item">
                                <div class="img">
                                    <img src="<?= $advantage['image'] ?>" alt="">
                                </div>
                                <h4 class="title"><?= $advantage['title'] ?></h4>
                                <?= $advantage['description'] ?>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="video-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="video">
                            <iframe src="<?= get_field('youtube_link') ?>" frameborder="0" allowfullscreen></iframe>

                        </div>
                        <h2 class="title">Ordina Ora a soli</h2>
                        <h4 class="title"><span>149,90€</span> <i class="fa fa-angle-right" aria-hidden="true"></i> <b>39,90€ + spedizione</b>, Paga all Consegna</h4>
                        <a href="#scrollform-bottom" class="button">ORDINA ADESSO</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="lettino-block-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
                            <h2 class="title">Caratteristiche Lettino Magico</h2>
                            <ul>
                                <li>Ventose super resistenti</li>
                                <li>I gatti lo adorano</li>
                                <li>Si monta subito</li>
                                <li>Portalo anche in vacanza</li>
                                <li>Pagamento alla consegna</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="block-4 magica-notte">
                        <div class="reviews">

                            <?php $connected = new WP_Query(array(
                                'connected_type' => 'products_to_reviews',
                                'connected_items' => get_queried_object(),
                                'nopaging' => true,
                            ));
                            ?>
                            <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                <div class="reviews-item hide-mobile">
                                    <div class="photo"
                                         style="background-image: url(<?= get_the_post_thumbnail_url() ?>)"></div>
                                    <h3 class="title"><?= get_the_title() ?></h3>
                                    <p><?= get_the_content() ?></p>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                            <div class="reviews-slider">
                                <?php $connected = new WP_Query(array(
                                    'connected_type' => 'products_to_reviews',
                                    'connected_items' => get_queried_object(),
                                    'nopaging' => true,
                                ));
                                ?>
                                <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                                    <div class="reviews-item">
                                        <div class="photo"
                                             style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                                        <h3 class="title"><?= get_the_title() ?></h3>
                                        <p><?= get_the_content() ?></p>
                                    </div>
                                <?php endwhile; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--style="background-image: url(<?= $image3 ?>); background-repeat: no-repeat !important;"-->
        <div class="bg-block-5 magica-notte"
             
             id="scrollform-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block-5">
                            <div class="forma bottom">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab3" data-toggle="tab">Passo 1</a>
                                    </li>
                                    <li>
                                        <a href="#tab4" class="next-step" data-toggle="tab">Passo 2</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?= do_shortcode($form_bottom) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END CONTENT-->
    <div class="dark-side-of-moon" style="display: none"></div>

<?php
get_footer();
