<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 15.11.16
 * Time: 14:14
 */

/**
 * Template name: Confirmation
 */

get_header();
if (isset($_GET['type']) && isset($_GET['param'])) {
    $pixels = filter_fb_pixels($_GET['type'], $_GET['param']);
    $payment_block = get_field('payment_block', $_GET['param']);
    $boolShowPix = $_GET['vph'];
}
?>

<section class="confirmation-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="confirmation">
                    <div class="description">
                        <img src="<?= get_template_directory_uri() ?>/images/logo.png" alt="">
                        <h2 class="title">Importante: Ancora Un Ultimo Passo...</h2>
                        <p>Controlla il tuo cellulare clicca sul link che stai per ricevere via SMS, altrimenti il
                            tuo ordine porterà dei ritardi.</p>
                        <p>Potresti ricevere anche una chiamata da un nostro operatore, quindi tieni sempre il
                            telefono vicino e stai pronto a rispondere.</p>

                        <p class="last">Vuoi pagare online senza dover pagare in contanti al postino? Clicca sul
                            tasto qui
                            sotto!</p>
                        <?php if (isset($payment_block)): ?>
                            <?= $payment_block ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if ($pixels && $boolShowPix) : ?>
    <?php foreach ($pixels as $pixel) : ?>

        <?= $pixel['comment_before'] ?>

        <iframe src="<?= $pixel['link'] ?>" scrolling="no" frameborder="0" width="1"
                height="1"></iframe>

        <?= $pixel['comment_after'] ?>

    <?php endforeach; ?>
<?php endif; ?>


<?php get_footer(); ?>

