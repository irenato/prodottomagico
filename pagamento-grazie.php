<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 07.12.16
 * Time: 11:30
 */

/**
 * Template name: Pagamento Grazie
 */

get_header();

?>

<section class="confirmation-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="confirmation">
                    <div class="description">
                        <img src="<?= get_template_directory_uri() ?>/images/logo.png" alt="">
                        <h2 class="title">Grazie! Il Tuo Ordine è Confermato</h2>
                        <p>Grazie per aver pagato il tuo ordine in anticipo. Adesso non ti resta che aspettare il prodotto che arriverà entro 2-3 giorni lavorativi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

